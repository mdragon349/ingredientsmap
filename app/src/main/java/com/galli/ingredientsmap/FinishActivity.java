package com.galli.ingredientsmap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class FinishActivity extends AppCompatActivity {

    private Recipe recipe;
    private TextView textViewAddress;
    private TextView textViewAcquista;
    FusedLocationProviderClient fusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        getRecipe();

        textViewAddress = findViewById(R.id.textview_indirizzo);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        textViewAcquista = findViewById(R.id.textViewAcquista);
    }

    private void getRecipe() {
        //mi riprendo il dato della ricetta
        Intent i = getIntent();
        recipe = (Recipe) i.getSerializableExtra("RECIPE");
    }

    public void findShippingAddress(View v) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(FinishActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Location location = task.getResult();
                if (location != null) {
                    Geocoder geocoder = new Geocoder(FinishActivity.this, Locale.getDefault());
                    try {
                        List<Address> addressList = geocoder.getFromLocation(
                                location.getLatitude(), location.getLongitude(), 1);
                        textViewAddress.setText("Indirizzo di spedizione:\n".concat(addressList.get(0).getAddressLine(0)));
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    public void buy(View v) {
        textViewAcquista.setText("L'ordine verra' elaborato entro 24h");

    }
    public void cancel(View v) {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);

    }

}