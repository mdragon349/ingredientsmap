package com.galli.ingredientsmap;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements RecipeAdapter.ItemClickListener{

    private RecyclerView recyclerView;
    private RecipeAdapter mAdapter;
    private ArrayList<Recipe> recipeList;
    private FirebaseFirestore db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recipeList = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler_ricette);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new RecipeAdapter(this, recipeList);
        mAdapter.setClickListener(this);
        recyclerView.setAdapter(mAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        db = FirebaseFirestore.getInstance();
        Task<QuerySnapshot> querySnapshotTask = db.collection("Recipe").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {


                        if (!queryDocumentSnapshots.isEmpty()) {

                            List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();

                            Log.e("Lista", "list: " + list);

                            for (DocumentSnapshot d : list) {

                                Recipe p = d.toObject(Recipe.class);
                                recipeList.add(p);

                            }

                            mAdapter.notifyDataSetChanged();

                        }
                    }
                }
                );
        Log.e("Lista", "Recipelist: " + recipeList);
    }



                    @Override
                    public void onItemClick(View view, int position) {
                        Recipe recipe = recipeList.get(position);
                        Log.e("Ricetta", "Ingredienti: " + recipe.getIngredients());
                        //  Log.v("RECIPE",recipe.getName()); visualizza nome di recipe nei log
                        Intent i = new Intent(this, RecipeViewerActivity.class);
                        i.putExtra("RECIPE", recipe);
                        startActivity(i);

                    }
                    // ...
                };