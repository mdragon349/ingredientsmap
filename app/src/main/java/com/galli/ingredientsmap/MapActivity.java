package com.galli.ingredientsmap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MapActivity  extends FragmentActivity implements OnMapReadyCallback {

    private Recipe recipe;
    private GoogleMap googleMap;
    private TextView textViewTotalCO2;
    FusedLocationProviderClient fusedLocationProviderClient;

    private List<Product> productList = new ArrayList<>();
    private LatLng userPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        textViewTotalCO2 = findViewById(R.id.textview_co2);
        getRecipe();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.recipemap);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    private void getRecipe() {
        //mi riprendo il dato della ricetta
        Intent i = getIntent();
        recipe = (Recipe) i.getSerializableExtra("RECIPE");
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        this.googleMap = googleMap;
        for (final String ingredient: recipe.getIngredients().keySet()) {
            db.collection("Product").document(ingredient.toLowerCase()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()){
                            Product product = document.toObject(Product.class);
                            productList.add(product);
                            createMarker(ingredient, product, googleMap);
                            Log.v("PRODUCT", product.getProducer());
                        } else {
                            Log.v("PRODUCT", ingredient + "not exists");
                        }
                    }
                }
            });
        }
    }

    private void createMarker(String ingredient, Product product, GoogleMap googleMap) {
        LatLng prod = new LatLng(Double.parseDouble(product.getLat()), Double.parseDouble(product.getLng()));
        googleMap.addMarker(new MarkerOptions().position(prod).title(product.getProducer() + ": " + ingredient));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(prod).zoom(10).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void findUserAddress(View v) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Location location = task.getResult();
                if (location != null) {
                    Geocoder geocoder = new Geocoder(MapActivity.this, Locale.getDefault());
                    try {
                        List<Address> addressList = geocoder.getFromLocation(
                                location.getLatitude(), location.getLongitude(), 1);
                        userPosition = new LatLng(addressList.get(0).getLatitude(), addressList.get(0).getLongitude());
                        googleMap.addMarker(new MarkerOptions().position(userPosition).title("La tua posizione").icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(userPosition).zoom(7).build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void calculateCO2Emissions(View v) {
        if (userPosition == null) {
            findUserAddress(this.getCurrentFocus());
            return;
        }

        // Controllo se la lista dei prodotti non è vuota
        if (productList.isEmpty()) {
            return;
        }

        float totalCO2emitted = 0;
        // Trovo la distanza per ogni prodotto
        for (Product product : productList) {
            Location userLocation = new Location("");
            Location productLocation = new Location("");
            userLocation.setLatitude(userPosition.latitude);
            userLocation.setLongitude(userPosition.longitude);
            productLocation.setLatitude(Double.parseDouble(product.getLat()));
            productLocation.setLongitude(Double.parseDouble(product.getLng()));
            float distanceInKm = userLocation.distanceTo(productLocation)/1000;
            float co2emitted = (distanceInKm * 260)/100;
            totalCO2emitted += co2emitted;
        }
        textViewTotalCO2.setText("Totale CO2 emessa: ".concat(String.valueOf(totalCO2emitted)).concat(" g/Km"));
        textViewTotalCO2.setVisibility(View.VISIBLE);
    }

}