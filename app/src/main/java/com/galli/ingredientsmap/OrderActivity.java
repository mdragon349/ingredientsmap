package com.galli.ingredientsmap;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.text.DecimalFormat;
import java.util.Map;

public class OrderActivity extends AppCompatActivity {

    private TextView textViewTitle;
    private EditText editTextPeopleNumber;
    private TextView textViewResult;
    private TextView textViewAddress;
    private Button btnContinue;
    private Recipe recipe;

    FusedLocationProviderClient fusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        getRecipe();
        textViewTitle = findViewById(R.id.textview_titolo);
        editTextPeopleNumber = findViewById(R.id.edittext_people);
        textViewResult = findViewById(R.id.textview_risultato);
        textViewAddress = findViewById(R.id.textview_indirizzo);
        btnContinue = findViewById(R.id.btn_continua);
        textViewTitle.setText("Stai ordinando:\n".concat(recipe.getName()));
        setButtonEnable(false);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    private void getRecipe() {
        //mi riprendo il dato della ricetta
        Intent i = getIntent();
        recipe = (Recipe) i.getSerializableExtra("RECIPE");
    }

    public void calculateIngredients(View v) {
        String finalIngredientsQuantity = "";
        String numberOfPeopleString = editTextPeopleNumber.getText().toString();
        if (numberOfPeopleString.equals("0") || numberOfPeopleString.isEmpty()) {
            editTextPeopleNumber.setError("Inserisci il numero di persone");
            editTextPeopleNumber.requestFocus();
            return;
        }

        int numberOfPeople = Integer.parseInt(numberOfPeopleString);
        for (Map.Entry<String, String> ingredient : recipe.getIngredients().entrySet()) {
            String name = ingredient.getKey();
            String quantity = ingredient.getValue();

            if (quantity.equals("qb")) {
                finalIngredientsQuantity = finalIngredientsQuantity.concat("\u25CF ").concat(name).concat(", ").concat("qb").concat("\n");
            } else {
                double qt = Double.parseDouble(quantity.split(" ")[0].trim());
                String type = quantity.split(" ")[1].trim();
                double result = qt * numberOfPeople;
                DecimalFormat df = new DecimalFormat("###.##");
                finalIngredientsQuantity = finalIngredientsQuantity.concat("\u25CF ").concat(name).concat(", ").concat(df.format(result)).concat(" ").concat(type).concat("\n");
            }

        }
        textViewResult.setText(finalIngredientsQuantity);
        setButtonEnable(true);
    }

    private void setButtonEnable(boolean isEnabled) {
        btnContinue.setClickable(isEnabled);
        btnContinue.setEnabled(isEnabled);
    }

    public void openFinishActivity(View v) {
        if (textViewResult.getText().equals("-")) {
            return;
        }
        Intent i = new Intent(this, FinishActivity.class);
        i.putExtra("RECIPE", recipe);
        startActivity(i);
    }

}