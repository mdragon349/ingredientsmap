package com.galli.ingredientsmap;

import java.util.List;

public class Product {

    private String lng;
    private String lat;
    private String producer;

    public Product() {
    }

    public Product(String lng, String lat, String producer) {
        this.lng = lng;
        this.lat = lat;
        this.producer = producer;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }
}
