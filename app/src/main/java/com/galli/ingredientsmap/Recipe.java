package com.galli.ingredientsmap;

import java.io.Serializable;
import java.util.HashMap;

public class Recipe implements Serializable {

    private String name;
    private String description;
    private String step;
    private String img;
    private HashMap<String, String> ingredients;


    public Recipe(String name, String description, String step, String img, HashMap<String, String> ingredients) {

        this.name = name;
        this.description = description;
        this.step = step;
        this.img = img;
        this.ingredients = ingredients;

    }

    public Recipe() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public HashMap<String, String> getIngredients() {
        return ingredients;
    }

    public String getIngredientsString(HashMap<String, String> ingredients) {
        StringBuilder ingredients_string = new StringBuilder();
        for (String ingredientName : ingredients.keySet()) {
            String name = ingredientName.substring(0, 1).toUpperCase().concat(ingredientName.substring(1));
            ingredients_string.append("\u25CF ").append(name).append(", ").append(ingredients.get(ingredientName)).append("\n");
        }
        return ingredients_string.toString();
    }

    public void setIngredients(HashMap<String, String> ingredients) {
        this.ingredients = ingredients;
    }
}










































