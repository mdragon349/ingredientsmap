package com.galli.ingredientsmap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.ViewHolder> {

    private List<Recipe> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    RecipeAdapter (Context context, List <Recipe> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;

    }

    @NonNull
    @Override
    public RecipeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.ingredient_listitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeAdapter.ViewHolder holder, int position) {
        Recipe recipe = mData.get(position);
        holder.textViewFoodName.setText(recipe.getName().substring(0,1).toUpperCase().concat(recipe.getName().substring(1)));
        holder.textViewDifficulty.setText(recipe.getDescription().substring(0, 1).toUpperCase().concat(recipe.getDescription().substring(1)));
        Picasso.get().load(recipe.getImg()).into(holder.imageviewFoodItem);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    //Definisco elementi template
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewFoodName;
        TextView textViewDifficulty;
        ImageView imageviewFoodItem;


        ViewHolder(View itemView) {
            super(itemView);
            textViewFoodName = itemView.findViewById(R.id.textView_foodname);
            textViewDifficulty= itemView.findViewById(R.id.textView_difficulty);
            imageviewFoodItem  = itemView.findViewById(R.id.imageview_fooditem);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
        }
    }
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


}


