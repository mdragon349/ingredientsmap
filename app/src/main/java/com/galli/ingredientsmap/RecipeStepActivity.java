package com.galli.ingredientsmap;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class RecipeStepActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_step);

        String recipeStep = getRecipeStep();
        TextView textView_step = findViewById(R.id.textview_step);
        textView_step.setText(recipeStep);

    }

    private String getRecipeStep() {
        //mi riprendo il dato della ricetta
        Intent i = getIntent();
        return i.getStringExtra("STEP");
    }
}