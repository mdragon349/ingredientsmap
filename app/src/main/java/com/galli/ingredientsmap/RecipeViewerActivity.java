package com.galli.ingredientsmap;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.zip.Inflater;


public class RecipeViewerActivity extends AppCompatActivity {

    private Recipe recipe;
    private TextView textview_ingredient;
    private ImageView img_recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_viewer);

        textview_ingredient = findViewById(R.id.textview_ingredients);
        img_recipe = findViewById(R.id.imageview_ricetta);
        getRecipe();
        setRecipe();
    }

    private void getRecipe() {
        //mi riprendo il dato della ricetta
        Intent i = getIntent();
        recipe = (Recipe) i.getSerializableExtra("RECIPE");
    }

    private void setRecipe () {
        TextView name = findViewById(R.id.textview_nome_ricetta);
        String recipeName = recipe.getName();
        name.setText(recipeName.substring(0,1).toUpperCase().concat(recipeName.substring(1)));
        Picasso.get().load(recipe.getImg()).into(img_recipe);
        textview_ingredient.setText(recipe.getIngredientsString(recipe.getIngredients()));
    }

    public void openStepActivity(View v) {
        Intent i = new Intent(this, RecipeStepActivity.class);
        i.putExtra("STEP", recipe.getStep());
        startActivity(i);
    }

    public void openOrderActivity(View v) {
        Intent i = new Intent(this, OrderActivity.class);
        i.putExtra("RECIPE", recipe);
        startActivity(i);
    }

    public void openMapActivity(View v) {
        Intent i = new Intent(this, MapActivity.class);
        i.putExtra("RECIPE", recipe);
        startActivity(i);
    }


}


